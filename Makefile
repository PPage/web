build-image:
	docker build -t registry.gitlab.com/ppage/web .

push-image:
	docker push registry.gitlab.com/ppage/web

build-and-push:
	make build-image
	make push-image