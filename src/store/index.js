const { omit, constant } = lodash

export const state = constant({
  user: {
    name: 'Artemii',
    surname: 'Kravtsov',
    position: 'Senior JavaScript Developer',
    company: {
      name: 'Booksy',
      link: '//booksy.com',
    },
    startOfExperience: 1456786800000, // 1 March 2016
    contacts: {
      linkedin: '//linkedin.com/in/artemii',
      email: 'artyomkravtsov@gmail.com',
    },
    technologies: [
      'css',
      'git',
      'graphql',
      'html',
      'js',
      'node',
      'sass',
      'vue',
      'cypress',
      'jest',
      'webpack',
    ],
  },
})

export const getters = {
  userData({ user }) {
    return omit(user, ['technologies'])
  },
  userTechnologies({ user }) {
    return user.technologies
  },
}
