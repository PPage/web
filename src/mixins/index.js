const { stubArray, partialRight, every, isString, includes } = lodash

export const optionsMixin = {
  props: {
    options: {
      type: Array,
      default: stubArray,
      validator: partialRight(every, isString),
    },
  },
  methods: {
    hasOption(option) {
      return includes(this.options, option)
    },
  },
}
