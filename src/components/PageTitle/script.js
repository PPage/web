export default {
  name: 'PageTitle',
  props: {
    text: {
      type: String,
      required: true,
    },
  },
  computed: {
    title() {
      return this.text.split(' ')
    },
  },
}
