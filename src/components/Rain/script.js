import { findIndex } from 'lodash/array'
import { uniqueId } from 'lodash/util'
import { sample } from 'lodash/collection'

import Drop from './Drop'
import { mapGetters } from 'vuex'

export default {
  name: 'Rain',
  components: { Drop },
  data() {
    return {
      drops: [],
    }
  },
  computed: mapGetters({
    technologies: 'userTechnologies',
  }),
  methods: {
    addDrop() {
      if (this.technologies.length) {
        const prefix = 'drop_'

        this.drops.push({
          name: sample(this.technologies),
          id: uniqueId(prefix),
        })
      }
    },
    removeDrop(idToRemove) {
      const index = findIndex(this.drops, this.findDropWithId(idToRemove))

      this.drops.splice(index, 1)
    },
    findDropWithId(dropId) {
      return ({ id }) => id === dropId
    },
  },
  mounted() {
    const timeout = window.innerWidth > 500 ? 300 : 450

    setInterval(this.addDrop, timeout)
  },
}
