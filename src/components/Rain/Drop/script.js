import { DROP_NAMES } from '~/plugins/consts'

const { random, reduce, merge } = lodash

const dropsReducer = (all, name) =>
  merge(all, { [name]: require(`@/assets/images/${name}.png`) })
const dropFiles = reduce(DROP_NAMES, dropsReducer, {})

export default {
  name: 'Drop',
  props: {
    drop: {
      type: Object,
      required: true,
    },
  },
  data() {
    return {
      isMounted: false,
      left: 0,
    }
  },
  computed: {
    src() {
      return dropFiles[this.drop.name]
    },
    dropStyle() {
      return {
        left: `${this.left}px`,
      }
    },
  },
  mounted() {
    this.left = random(0, window.innerWidth - 100)

    this.isMounted = true

    setTimeout(this.removeDrop, 10000)
  },
  methods: {
    removeDrop() {
      this.$emit('removeDrop', this.drop.id)
    },
  },
}
