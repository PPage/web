import { isObject } from 'lodash/lang'

import { optionsMixin } from '@/mixins'

export default {
  name: 'ExternalLink',
  mixins: [optionsMixin],
  props: {
    link: {
      type: String,
      required: true,
    },
    text: {
      type: String,
      required: true,
    },
    params: {
      type: Array,
      default: () => [],
      validator: value => value.every(isObject),
    },
  },
  computed: {
    parsed() {
      const restrictedStrings = ['mailto']
      let isCustom = false

      restrictedStrings.forEach(string => {
        if (this.link.indexOf(string) !== -1) {
          isCustom = true
        }
      })

      const prefix = !isCustom ? '//' : ''

      return `${prefix}${this.link}${this.parameters}`
    },
    target() {
      const target = this.hasOption('currentTab') ? 'self' : 'blank'

      return `_${target}`
    },
    parameters() {
      return this.params.map(this.prepareParameter).join('&')
    },
  },
  methods: {
    prepareParameter(param, index) {
      const isFirst = index === 0
      const prefix = isFirst ? '?' : ''

      return `${prefix}${param.name}=${param.value}`
    },
  },
}
