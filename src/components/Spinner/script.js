import { times } from 'lodash/util'

export default {
  name: 'Spinner',
  computed: {
    dots() {
      return times(12, (index) => index + 1)
    },
  },
  methods: {
    dotClass(dot) {
      return `sk-circle${dot}`
    },
  },
}
