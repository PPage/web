export default {
  name: 'Button',
  props: {
    title: {
      type: String,
      required: true,
    },
    action: {
      type: Function,
      required: true,
    },
  },
}
