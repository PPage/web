const productionTip = 'production'
const isProduction = process.env.NODE_ENV === productionTip

export const BASE_URL = isProduction
  ? '//api.artemii.space'
  : '//localhost:8000'

export const DROP_NAMES = [
  'apache',
  'css',
  'cypress',
  'git',
  'github',
  'gitlab',
  'go',
  'graphql',
  'html',
  'java',
  'jest',
  'js',
  'nginx',
  'node',
  'sass',
  'svn',
  'ts',
  'vue',
  'webpack',
]
