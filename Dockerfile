FROM node:10-alpine

RUN apk add g++ make python

ENV APP_ROOT /src
ENV NODE_ENV production

WORKDIR ${APP_ROOT}
ADD . ${APP_ROOT}

RUN npm ci
RUN npm run build

CMD ["npm", "run", "start"]
