import webpack from 'webpack'

const googleAnalytics = [
  '@nuxtjs/google-analytics',
  {
    id: 'UA-132859765-1',
  },
]

export default {
  buildModules: [googleAnalytics],
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        lodash: 'lodash',
      }),
    ],
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        })
      }
    },
  },
  srcDir: 'src/',
  css: ['@/assets/styles/global.sass'],
  head: {
    title: 'Artemii Kravtsov — a JS expert',
  },
}
